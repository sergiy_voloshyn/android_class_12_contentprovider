package com.sourceit.databaseprovider;

import android.net.Uri;

public interface Table {

    String getName();

    String getColumnId();

    Uri getItemUri(long id);

}